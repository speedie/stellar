#!/bin/sh

test *.patch && rm -f *.patch && echo "Deleted old patches"
pname="genstellar-$(date +%D | sed "s|/|_|g").patch"
diff -up stellar.orig stellar_patched > $pname
sed -i "s/stellar.orig/status/g" $pname
sed -i "s/stellar_patched/stellar/g" $pname && echo "Created patch @ $pname"
echo "$pname" > .latestpatch

#!/bin/sh
# patchlatest.sh
# this script downloads the latest status script and applies the stellar patch to it. this is not guaranteed to succeed.

clear
echo "patchlatest.sh"
echo "--------------"
echo "Do you want to attempt to patch the latest status from speedwm main?"
echo "This action is likely to fail if the patch is out of date. (y/n)"
echo -n "> " ; read act

if [ "$act" = "y" ]; then
		clear
else
		echo "Stopping..." && exit 1
fi

# patches and status
#stellar_patch="https://codeberg.org/speedie/stellar/raw/branch/patch/genstellar-01_09_2022.patch"
stellar_patch="https://codeberg.org/speedie/stellar/raw/branch/patch/$(curl -s https://codeberg.org/speedie/stellar/raw/branch/patch/.latestpatch)"
stellar_unpatched="https://codeberg.org/speedie/speedwm/raw/branch/master/status"

# download
curl -so status $stellar_unpatched || exit 1
curl -so patch $stellar_patch || exit 1

if [ -e "$(basename $stellar_patch)" ]; then
		echo "Downloaded patch successfully"
fi

# check if a status was downloaded
if [ -e "status" ]; then
		echo "Downloaded status successfully"
fi

cp status stellar.orig

# patch
patch < patch

# check if it failed
if [ -e "status.rej" ]; then
		mv status.rej stellar.rej
		fail=true
fi

# move status
mv status stellar_patched

# move orig file
if [ -e "status.orig" ]; then
		mv status.orig stellar.orig
fi

# remove patch
rm -f patch

echo "---------------------------------------"
if [ "$fail" = "true" ]; then
echo "Failed to automatically patch the status. This is not too uncommon. Patch it manually or grab a stable version."
else
echo "Patched the status automatically. Run install.sh to install the patched stellar."
fi

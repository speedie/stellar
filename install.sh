#!/bin/sh

# check if we're running as root.
if [ "$(whoami)" != "root" ]; then
		echo "Run install.sh as root."
		exit 1
fi

# if stellar exists remove and back it up
if [ -e "/usr/bin/stellar" ]; then
		echo "stellar already installed, removing and backing it up to $(pwd)/stellar.old"
		cp /usr/bin/stellar ./stellar.old
fi

# install stellar
if [ -e "stellar_patched" ]; then
cp stellar_patched /usr/bin/stellar
else
cp stellar /usr/bin
fi

# change permissions if it exists
if [ -e "/usr/bin/stellar" ]; then
		chmod +x /usr/bin/stellar
		echo "Installed stellar."
else
		echo "Unable to install stellar."
		exit 1
fi
